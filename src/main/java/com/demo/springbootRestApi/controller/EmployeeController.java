package com.demo.springbootRestApi.controller;


import com.demo.springbootRestApi.exception.ResourceNotFoundException;
import com.demo.springbootRestApi.model.Employee;
import com.demo.springbootRestApi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Employee> getAllEmployees(){
        return employeeRepository.findAll();

    }

    @PostMapping
    @RequestMapping("/newEmployee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee){
        Employee emp = employeeRepository.save(employee);

        return ResponseEntity.ok(emp);
    }

    @GetMapping("{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable long id){
        Employee employee = employeeRepository.findById(id).orElseThrow(() ->
            new ResourceNotFoundException("Employee Not Found with id "+ id));

        return ResponseEntity.ok(employee);

    }

    @PutMapping("{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable long id, @RequestBody Employee updateEmployee){
        Employee emp = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("No employee details found for "+id));
        emp.setFirst_name(updateEmployee.getFirst_name());
        emp.setLast_name(updateEmployee.getLast_name());
        emp.setEmail(updateEmployee.getEmail());

        employeeRepository.save(emp);

        return ResponseEntity.ok(emp);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> deleteEmployee(@PathVariable long id){
        Employee emp = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Employee not found with id "+id));
        employeeRepository.delete(emp);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
