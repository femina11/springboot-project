package com.demo.springbootRestApi.controller;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.demo.springbootRestApi.model.Employee;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RestClient {

    private static final String GET_ALL_EMPLOYEES = "http://localhost:8080/api/employees";
    private static final String GET_EMPLOYEE_BY_ID = "http://localhost:8080/api/employees/{id}";
    private static final String CREATE_EMPLOYEE = "http://localhost:8080/api/employees/newEmployee";
    private static final String UPDATE_EMPLOYEE = "http://localhost:8080/api/employees/{id}";
    private static final String DELETE_EMPLOYEES = "http://localhost:8080/api/employees/{id}";

    static RestTemplate restTemplate = new RestTemplate();
    public static void main(String[] args) {
        callCreateEmployee();
        callGetAllEmployees();
        callGetEmployeeById();
        callUpdateEmployee();
        callDeleteEmployee();
        callGetAllEmployees();
    }

    private static void callGetAllEmployees(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);

        ResponseEntity<String> result = restTemplate.exchange(GET_ALL_EMPLOYEES, HttpMethod.GET, entity, String.class);

        System.out.println(result);

    }

    private static void callGetEmployeeById(){
        Map<String, Integer> params= new HashMap<>();
        params.put("id", 10);

        Employee emp = restTemplate.getForObject(GET_EMPLOYEE_BY_ID, Employee.class, params);

        System.out.println(emp);

    }

    private static void callCreateEmployee(){
        Employee emp = new Employee("George", "Fernandece", "georgef@gmail.com");
        ResponseEntity<Employee> result = restTemplate.postForEntity(CREATE_EMPLOYEE, emp, Employee.class);
        System.out.println("Created new employee");
        System.out.println(result.getBody());
    }

    private static void callUpdateEmployee(){
        Map<String, Integer> param = new HashMap<>();
        param.put("id", 2);
        Employee updatedEmp = new Employee("Geo", "user", "georgef@gmail.com");
        restTemplate.put(UPDATE_EMPLOYEE, updatedEmp, param);
    }

    private static void callDeleteEmployee(){
        Map<String, Integer> param = new HashMap<>();
        param.put("id", 5);
        restTemplate.delete(DELETE_EMPLOYEES, param);
    }

}
