package com.demo.springbootRestApi;

import com.demo.springbootRestApi.model.Employee;
import com.demo.springbootRestApi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringbootRestApiApplication {

	public static void main(String[] args) { SpringApplication.run(SpringbootRestApiApplication.class, args); }

//    @Autowired
//    private EmployeeRepository employeeRepository;
//
//    @Override
//    public void run(String... args) throws Exception {
//        Employee emp = new Employee("John", "Dev", "jhond@gmail.com");
//
//        employeeRepository.save(emp);
//
//        Employee emp1 = new Employee("Daniel", "Rob", "danielr @gmail.com");
//
//        employeeRepository.save(emp1);
//
//    }
}
