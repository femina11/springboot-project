package com.demo.springbootRestApi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "EMP")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private long id;
    @Column(name = "FIRSTNAME")
    private String first_name;
    private String last_name;
    private String email;

    public Employee(String first_name, String last_name, String email) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
    }
}
